package uz.ogabek_dev.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileService {
    private static final String SORTED_FILENAME = "sorted_version.txt";
    private static final String GIVEN_FILENAME = "version.txt";
    private List<List<String>> versions;

    /**
     * Sort version ASC call from MAIN class
     **/
    public void sortVersionASC() throws IOException {

//        Delete sorted file if exist
        Files.deleteIfExists(Path.of(SORTED_FILENAME));

//        Logic area for read, sort and write
        readFileToList();
        versions
                .stream()
                .sorted(this::compareLists)
                .map(stringList -> String.join(".", stringList))
                .peek(this::writeToFile)
                .toList()
                .forEach(System.out::println);
    }

    /**
     * Compare each line version for Stream sort comparator
     **/
    private int compareLists(List<String> o1, List<String> o2) {
        for (int i = 0; i < Math.min(o1.size(), o2.size()); i++) {
            int e1 = Integer.parseInt(o1.get(i));
            int e2 = Integer.parseInt(o2.get(i));
            if (e1 != e2)
                return e1 > e2 ? 1 : -1;
        }
        return o1.size() > o2.size() ? 1 : -1;
    }

    /**
     * Read file from version.txt and add versions list
     **/
    private void readFileToList() {
        try {
            File myObj = new File(GIVEN_FILENAME);
            Scanner myReader = new Scanner(myObj);
            versions = new ArrayList<>();
            while (myReader.hasNextLine()) {
                String version = myReader.nextLine();
                versions.add(List.of(version.split("\\.")));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.printf("An error occurred. Message: [%s]", e.getMessage());
        }
    }

    /**
     * Write the given String (version) to a file
     **/
    private void writeToFile(String version) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(SORTED_FILENAME, true))) {
            writer.write(version);
            writer.newLine();
        } catch (IOException e) {
            System.out.printf("An error occurred. Message: [%s]", e.getMessage());
        }
    }
}
