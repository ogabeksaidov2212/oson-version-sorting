package uz.ogabek_dev;

import uz.ogabek_dev.service.FileService;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        FileService fileService = new FileService();

        System.out.println("""
                
                ==========================
                =      Hello there !!    =
                =  It's my task for OSON =
                =       Let's goo...     =
                ==========================
                """);
        System.out.println("""
                
                Choose one of the options below:
                1.  Sort version by ASC
                0.  Don't sort and EXIT
                
                """);
        System.out.print("Option: ");
        int option = scanner.nextInt();
        switch (option) {
            case 1 -> fileService.sortVersionASC();
            case 0 -> System.exit(1);
            default -> System.out.println("You are stupid, Do not be mazgi and develop your brain !!");
        }
    }
}
